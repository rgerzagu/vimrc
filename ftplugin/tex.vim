

" section jumping
noremap <buffer> <silent> ]] :<c-u>call TexJump2Section( v:count1, '' )<CR>
noremap <buffer> <silent> [[ :<c-u>call TexJump2Section( v:count1, 'b' )<CR>
function! TexJump2Section( cnt, dir )
  let i = 0
  let pat = '^\s*\\\(part\|chapter\|\(sub\)*section\|paragraph\)\>\|\%$\|\%^'
   let flags = 'W' . a:dir
   while i < a:cnt && search( pat, flags ) > 0
     let i = i+1
   endwhile
   let @/ = pat
 endfunction



"call IMAP("/e","é","tex")
call IMAP ("/em" , "\\emph{<++>} <++>" , "tex")
call IMAP ("/s3" , "\\subsubsection{<++>}\<CR><++>" , "tex")
call IMAP ("/s2" , "\\subsection{<++>}\<CR><++>" , "tex")
call IMAP ("/s1" , "\\section{<++>}\<CR><++>" , "tex")
"call IMAP ("/p" , "\\paragraph{<++>}<++>" , "tex")
call IMAP (",bit", "\\begin{itemize}\<cr>\\item <++>\<cr>\\end{itemize}\<cr><++>", "tex")
call IMAP (",num", "\\begin{enumerate}\<cr>\\item <++>\<cr>\\end{enumerate}\<cr><++>", "tex")
call IMAP(",footnote", "\\footnote{ <++> } <++>", "tex")
"call IMAP("\item", "\\item <++>", "tex")
call IMAP(",mf", "\\mfst{}", "tex")
call IMAP(",frac", "\\frac{<++>}{<++>}", "tex")
call IMAP(",fig", "\\begin{figure}[ht!]\<cr>\\centering\<cr>\\includegraphics[scale=<++>]{<++>}\<cr>\\caption{<++>}\<cr>\\label{<++>}\<cr>\\end{figure}", "tex")
call IMAP(",tikz", "\\begin{figure}[ht!]\<cr>\\centering\<cr>\\input{<++>}\<cr>\\caption{<++>}\<cr>\\label{<++>}\<cr>\\end{figure}", "tex")
call IMAP(",frame","\\begin{frame}{<++>}\<cr><++>\<cr>\\end{frame}\<cr>","tex")
call IMAP(",vv","\\vfill","tex")
call IMAP("**","\\cdot ","tex")
call IMAP("||","\\vert <++> \\vert\^2 <++>","tex")



"map <F6> :!pdflatex -synctex=1 -file-line-error-style -interaction=nonstopmode %<cr>
nnoremap ,nn i\begin{equation}<cr><++><cr>\end{equation}<cr><++>
nnoremap ,ns i\begin{equation*}<cr>\begin{split} <cr> <++><cr>\end{split}<cr>\end{equation*}<cr><++>
nnoremap ,ii i\begin{itemize}<cr>\item <++><cr>\end{itemize}<cr><++>
nnoremap ,it i\item <++>
nnoremap ,ff i\begin{frame}{<++>}<cr><++><cr>\end{frame}<cr>
nnoremap ,ee i\begin{center}<cr><++><cr>\end{center}<cr><++>
nnoremap ,nn i\begin{equation}<cr><++><cr>\end{equation}<cr><++>
nnoremap ,qq i\begin{eqnarray*}<cr><++><cr>\end{eqnarray*}<cr><++>
nnoremap ,cc i\begin{lstlisting}<cr><++><cr>\end{lstlisting}<cr><++>
nnoremap ,bb i\textbf{<++>}<++>
nnoremap ,cp :!pdflatex -synctex=1 -file-line-error-style -interaction=nonstopmode %<cr>
nnoremap ,oo :!open -a Skim "%<.pdf" &<CR>

imap ,nn \begin{equation}<cr><++><cr>\end{equation}<cr><++>


"call IMAP('é','é','tex')


"let g:tex_fold_enabled=1

" to have alt wihtout set the menu in gui
set winaltkeys=no


" --- Special Paragraph style for Comment
imap ,-- % ---------------------------------------------------- <cr>% --- <++> <cr>% ---------------------------------------------------- <cr>

" Changing in visual mode
vnoremap <buffer> <leader>bb "zc\textbf{<C-R>z}<Esc>
vnoremap <buffer> <leader>tt "zc\student{<C-R>z}<Esc>
vnoremap <buffer> <leader>it "zc\textit{<C-R>z}<Esc>
vnoremap <buffer> <leader>ss "zc\underline{<C-R>z}<Esc>
vnoremap <buffer> <leader>mf "zc\mfst{<C-R>z}<Esc>
vnoremap <buffer> <leader>mfg "zc\mfstg{<C-R>z}<Esc>
vnoremap <buffer> <leader>mfp "zc\mfstp{<C-R>z}<Esc>
vnoremap <buffer> <leader>mfe "zc\mfste{<C-R>z}<Esc>
vnoremap <buffer> <leader>mfv "zc\mfstv{<C-R>z}<Esc>
vnoremap <buffer> <leader>mfr "zc\mfstr{<C-R>z}<Esc>


vnoremap <buffer> <c-b> "zc\textbf{<C-R>z}<Esc>
vnoremap <buffer> <c-i> "zc\textit{<C-R>z}<Esc>
vnoremap <buffer> <c-u> "zc\underline{<C-R>z}<Esc>

imap <buffer> <leader>it <Plug>Tex_InsertItemOnThisLine
imap <C-t> \textit{}
imap <C-b> \textbf{}
imap <C-s> \underline{}
imap <C-a> jkA

imap <leader>vs \vspace{1cm}

" Automatically set current file to master file
function! SetMaster()
	let s:syncfile = fnamemodify(fnameescape(Tex_GetMainFileName()), ":r").".tex.latexmain"
	let execstr = "silent !touch ".s:syncfile.' &'
	exec execstr
endfunction
function! ClearMaster()
	let s:syncfile = fnamemodify(fnameescape(Tex_GetMainFileName()), ":r").".tex.latexmain"
	let execstr = "silent !rm ".s:syncfile.' &'
	exec execstr
endfunction
nnoremap <Leader>SM :call SetMaster()<CR>
nnoremap <Leader>CM :call ClearMaster()<CR>

" Saving before compilation
map <Leader>pp :w<cr><leader>ll

" Compile target file even when SM
function! CompileTargetFile()
	" Get working directory
	let g:sessiondir		  = getcwd()
	" get Filename and compile dir
	let s:syncfile			  = expand("%:p")
	let g:currentCompilDir	  = expand("%:p:h")
	" go to compil dir
	let execstr				  = "cd ".g:currentCompilDir
	exec execstr
	" Compilation
	let execstr = "silent !pdflatex -synctex=1 -interaction=nonstopmode ".s:syncfile " &"
	exec execstr
	" Go back to working dir
	let execstr				  = "cd ".g:sessiondir
	exec execstr
endfunction
nnoremap <Leader>lp :w<cr>:call CompileTargetFile() <cr>


" Compile target file even when SM
function! CompileVerboseFile()
	" Get working directory
	let g:sessiondir		  = getcwd()
	" get Filename and compile dir
	let s:syncfile			  = expand("%:p")
	let g:currentCompilDir	  = expand("%:p:h")
	" go to compil dir
	let execstr				  = "cd ".g:currentCompilDir
	exec execstr
	" Compilation
	let execstr = "!pdflatex -synctex=1 -interaction=nonstopmode ".s:syncfile 
	exec execstr
	" Go back to working dir
	let execstr				  = "cd ".g:sessiondir
	exec execstr
endfunction



"prevent crazy indentation in case of [ { , ...
let g:tex_indent_brace=0

set iskeyword+=:,_


" Convert PDF file (that have been compiled) into PNG file
function! ConvertPNG()
	" get Filename and compile dir
	let s:syncfile			  = expand("%:r")
	" Convertion into PNG 
	let execstr = "!convert -quality 400 -density 400 ".s:syncfile.".pdf ".s:syncfile.".png"
	"echo execstr
	exec execstr
endfunction


" Base for tikz
imap ,baseTIKZ \documentclass[landscape,preview]{standalone}<cr>\usepackage[T1]{fontenc}<cr>\usepackage[utf8]{inputenc}<cr>\usepackage{tikz}<cr>\usetikzlibrary{shapes.geometric,shapes.arrows,decorations.pathmorphing}<cr>\usetikzlibrary{matrix,chains,scopes,positioning,arrows,fit}<cr>\usepackage{schemabloc}<cr>\usetikzlibrary{circuits}<cr>\usepackage{verbatim}<cr>\usepackage{caption}<cr>\usepackage{bm}<cr>%\usepackage{pdflscape}<cr>\usepackage[landscape]{geometry}<cr>\usepackage{pgfplots}<cr>%Commande comparateurs et sommateurs<cr>\definecolor{GIPSAgrisc}{rgb}{0.9,0.9,0.9}<cr>\definecolor{GIPSAgrisf}{rgb}{0.4,0.4,0.4}<cr>\begin{document}<cr>\thispagestyle{empty}<cr>\begin{tikzpicture}<cr><cr><cr>\end{tikzpicture}<cr> \end{document}

imap ,scomp \newcommand{\sComp}[7][4]{ <cr>	\node [draw, circle,minimum size=1em, right of=#3,line width=1pt,node distance=#1em] (#2) {};<cr>	\node [draw, cross out,minimum size=0.707em,right of=#3,line width=1pt,node distance=#1em] {};<cr>	\node [above of=#2,node distance=0.6em] {$#4$};<cr>	\node [below of=#2,node distance=0.6em] {$#5$};<cr>	\node [left of=#2,node distance=0.6em] {$#6$};<cr>	\node [right of=#2,node distance=0.6em] {$#7$};<cr>}


imap ,ex \begin{exampleblock}{\textbf{<++>}}<cr> <++><cr> \end{exampleblock}<cr>

imap ,ff \begin{frame}{<++>}<cr><++><cr>\end{frame}<cr>

imap ,gg jk{jwwwwwv$hhy}kjji\begin{frame}{jkpa}<cr><++><cr>\end{frame}


" Replace () by []
noremap <leader>( %r)jk``r(
noremap <leader>[ %r]jk``r[

" remove \textit{} or \corr{} form text
nmap <leader>D ds}dF\ 

" Adding itmize in selection
vnoremap <buffer> <leader>it "zc\begin{itemize}<cr>\item<C-R><cr>\end{itemize}<cr>z<Esc>

" Comment % ---
imap <leader>% % --- 


" remove \textit{} or \corr{} form text
nmap <leader>D ds}dF\ 

" Replace () by []
noremap <leader>( %r)jk``r(
noremap <leader>[ %r]jk``r[


"Problem of â in Latex
imap <buffer> <leader>mb <Plug>Tex_MathBF


imap ,ref \ref{fig:<++>}<c-j>
imap ,req \ref{eq:<++>}<c-j>
imap ,/ \frac{<++>}{<++>}<c-j>


"
"
"
"
"
"
"
"" section jumping
"noremap <buffer> <silent> ]] :<c-u>call TexJump2Section( v:count1, '' )<CR>
"noremap <buffer> <silent> [[ :<c-u>call TexJump2Section( v:count1, 'b' )<CR>
"function! TexJump2Section( cnt, dir )
  "let i = 0
  "let pat = '^\s*\\\(part\|chapter\|\(sub\)*section\|paragraph\)\>\|\%$\|\%^'
   "let flags = 'W' . a:dir
   "while i < a:cnt && search( pat, flags ) > 0
     "let i = i+1
   "endwhile
   "let @/ = pat
 "endfunction




""map <F6> :!pdflatex -synctex=1 -file-line-error-style -interaction=nonstopmode %<cr>
"nnoremap ,nn i\begin{equation}<cr><++><cr>\end{equation}<cr><++>
"nnoremap ,ns i\begin{equation*}<cr>\begin{split} <cr> <++><cr>\end{split}<cr>\end{equation*}<cr><++>
"nnoremap ,ii i\begin{itemize}<cr>\item <++><cr>\end{itemize}<cr><++>
"nnoremap ,it i\item <++>
"nnoremap ,ff i\begin{frame}{<++>}<cr><++><cr>\end{frame}<cr>
"nnoremap ,ee i\begin{center}<cr><++><cr>\end{center}<cr><++>
"nnoremap ,nn i\begin{equation}<cr><++><cr>\end{equation}<cr><++>
"nnoremap ,qq i\begin{eqnarray*}<cr><++><cr>\end{eqnarray*}<cr><++>
"nnoremap ,cc i\begin{lstlisting}<cr><++><cr>\end{lstlisting}<cr><++>
"nnoremap ,bb i\textbf{<++>}<++>
"nnoremap ,cp :!pdflatex -synctex=1 -file-line-error-style -interaction=nonstopmode %<cr>
"nnoremap ,oo :!okular "%<.pdf" &<CR>


""call IMAP('é','é','tex')


"let g:tex_fold_enabled=1

"" to have alt wihtout set the menu in gui
"set winaltkeys=no


"" --- Special Paragraph style for Comment
"imap ,-- % ---------------------------------------------------- <cr>% --- <++> <cr>% ---------------------------------------------------- <cr>

"" Changing in visual mode
"vnoremap <buffer> <leader>bb "zc\textbf{<C-R>z}<Esc>
"vnoremap <buffer> <leader>it "zc\textit{<C-R>z}<Esc>
"vnoremap <buffer> <leader>ss "zc\underline{<C-R>z}<Esc>
"vnoremap <buffer> <leader>mf "zc\mfst{<C-R>z}<Esc>
"vnoremap <buffer> <leader>mfg "zc\mfstg{<C-R>z}<Esc>
"vnoremap <buffer> <leader>mfp "zc\mfstp{<C-R>z}<Esc>
"vnoremap <buffer> <leader>mfe "zc\mfste{<C-R>z}<Esc>
"vnoremap <buffer> <leader>mfv "zc\mfstv{<C-R>z}<Esc>
"vnoremap <buffer> <leader>mfr "zc\mfstr{<C-R>z}<Esc>


"vnoremap <buffer> <c-b> "zc\textbf{<C-R>z}<Esc>
"vnoremap <buffer> <c-i> "zc\textit{<C-R>z}<Esc>
"vnoremap <buffer> <c-u> "zc\underline{<C-R>z}<Esc>

"imap <buffer> <leader>it <Plug>Tex_InsertItemOnThisLine
"imap <C-t> \textit{}
"imap <C-b> \textbf{}
"imap <C-s> \underline{}
"imap <C-a> jkA



"" Automatically set current file to master file
"function! SetMaster()
	"let s:syncfile = fnamemodify(fnameescape(Tex_GetMainFileName()), ":r").".tex.latexmain"
	"let execstr = "silent !touch ".s:syncfile.' &'
	"exec execstr
"endfunction
"function! ClearMaster()
	"let s:syncfile = fnamemodify(fnameescape(Tex_GetMainFileName()), ":r").".tex.latexmain"
	"let execstr = "silent !rm ".s:syncfile.' &'
	"exec execstr
"endfunction
"nnoremap <Leader>SM :call SetMaster()<CR>
"nnoremap <Leader>CM :call ClearMaster()<CR>

"" Saving before compilation
"map <Leader>pp :w<cr><leader>ll

"" Compile target file even when SM
"function! CompileTargetFile()
	"" Get working directory
	"let g:sessiondir		  = getcwd()
	"" get Filename and compile dir
	"let s:syncfile			  = expand("%:p")
	"let g:currentCompilDir	  = expand("%:p:h")
	"" go to compil dir
	"let execstr				  = "cd ".g:currentCompilDir
	"exec execstr
	"" Compilation
	"let execstr = "silent !pdflatex -synctex=1 -interaction=nonstopmode ".s:syncfile " &"
	"exec execstr
	"" Go back to working dir
	"let execstr				  = "cd ".g:sessiondir
	"exec execstr
"endfunction
"nnoremap <Leader>lp :w<cr>:call CompileTargetFile() <cr>


"" Compile target file even when SM
"function! CompileVerboseFile()
	"" Get working directory
	"let g:sessiondir		  = getcwd()
	"" get Filename and compile dir
	"let s:syncfile			  = expand("%:p")
	"let g:currentCompilDir	  = expand("%:p:h")
	"" go to compil dir
	"let execstr				  = "cd ".g:currentCompilDir
	"exec execstr
	"" Compilation
	"let execstr = "!pdflatex -synctex=1 -interaction=nonstopmode ".s:syncfile 
	"exec execstr
	"" Go back to working dir
	"let execstr				  = "cd ".g:sessiondir
	"exec execstr
"endfunction



""prevent crazy indentation in case of [ { , ...
"let g:tex_indent_brace=0

"set iskeyword+=:,_


"" Base for tikz
"imap ,baseTIKZ \documentclass[landscape,preview]{standalone}<cr>\usepackage[T1]{fontenc}<cr>\usepackage[utf8]{inputenc}<cr>\usepackage{tikz}<cr>\usepackage{pgfplots}<cr>\usetikzlibrary{shapes.geometric,shapes.arrows,decorations.pathmorphing}<cr>\usetikzlibrary{matrix,chains,scopes,positioning,arrows,fit}<cr>\usepackage{schemabloc}<cr>\usetikzlibrary{circuits}<cr>\usepackage{verbatim}<cr>\usepackage{caption}<cr>\usepackage{bm}<cr>%\usepackage{pdflscape}<cr>\usepackage[landscape]{geometry}<cr>%Commande comparateurs et sommateurs<cr>\definecolor{GIPSAgrisc}{rgb}{0.9,0.9,0.9}<cr>\definecolor{GIPSAgrisf}{rgb}{0.4,0.4,0.4}<cr>\begin{document}<cr>\thispagestyle{empty}<cr>\begin{tikzpicture}<cr><cr><cr>\end{tikzpicture}<cr> \end{document}

"imap ,scomp \newcommand{\sComp}[7][4]{ <cr>	\node [draw, circle,minimum size=1em, right of=#3,line width=1pt,node distance=#1em] (#2) {};<cr>	\node [draw, cross out,minimum size=0.707em,right of=#3,line width=1pt,node distance=#1em] {};<cr>	\node [above of=#2,node distance=0.6em] {$#4$};<cr>	\node [below of=#2,node distance=0.6em] {$#5$};<cr>	\node [left of=#2,node distance=0.6em] {$#6$};<cr>	\node [right of=#2,node distance=0.6em] {$#7$};<cr>}


"imap ,ex \begin{exampleblock}{\textbf{<++>}}<cr> <++><cr> \end{exampleblock}<cr>

"imap ,ff \begin{frame}{<++>}<cr><++><cr>\end{frame}<cr>
"" Custom Latex mappings


"imap ,bit \begin{itemize}<cr>\item<++><cr>\end{itemize}<cr><++>
"imap ,mf \mfst{<++>}<cr>
"imap `/ \frac{<++>}{<++>}<++>
"imap ,fig \begin{figure}<cr>\centering<cr>\includegraphics[scale=<++>]{<++>}<cr>\caption{<++>}<cr>\label{<++>}<cr>\end{figure}
"imap ,tikz \begin{figure}[ht!]<cr>\centering<cr>\input{<++>}<cr>\caption{<++>}<cr>\label{<++>}<cr>\end{figure}
"imap ,ff \begin{frame}{<++>}<cr><++><cr>\end{frame}<cr>
"imap ,vv \vfill
""imap ,|| \vert <++> \vert ^2

"" Replace () by []
"noremap <leader>( %r)jk``r(
"noremap <leader>[ %r]jk``r[

"" remove \textit{} or \corr{} form text
"nmap <leader>D ds}dF\ 

"" Adding itmize in selection
"vnoremap <buffer> <leader>it "zc\begin{itemize}<cr>\item<C-R><cr>\end{itemize}<cr>z<Esc>

"" Comment % ---
"imap <leader>% % --- 


"" remove \textit{} or \corr{} form text
"nmap <leader>D ds}dF\ 

"" Replace () by []
"noremap <leader>( %r)jk``r(
"noremap <leader>[ %r]jk``r[


""Problem of â in Latex
"imap <buffer> <leader>mb <Plug>Tex_MathBF
