" -------------------------------------------------
"  vimrc : VIM Configuration file
"  R. Gerzaguet
"  ------------------------------------------------
"
" v 1.3 : Apr. 1014
" This file has been inspired by Gardouille


" --- I recommand to use Pathogen for plugin management
" 		* Installation :  sudo apt-get install curl 
"						  curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
"		* Then simply install plugins by cloning git repo in ~/.vim/bundle
"						  cd ~/.vim/bundle/ 
"						  ---  nerdtred : easy navigation in subdir files (one tree to rule them all)
"						  git clone https://github.com/scrooloose/nerdtree.git
"						  git clone https://github.com/jistr/vim-nerdtree-tabs.git
"						  --- syntastic : on the fly checker 
"						  git clone https://github.com/scrooloose/syntastic
"						  --- vim airline : fuzzy customized statebar 
"						  git clone https://github.com/bling/vim-airline.git
"						  --- latex-suite : powerfull latex for vim
"						  git clone https://github.com/vim-latex/vim-latex.git 
"						  --- acp : automatic completion frame
"						  git clone  https://github.com/vim-scripts/AutoComplPop.git
"						  --- Supertab : smart completion on tab
"						  git clone https://github.com/ervandew/supertab.git
"						  --- Ultisnips : automatic code completion
"						  git clone https://github.com/sirver/ultisnips
"						  --- Surround : parenthesizing made simple 
"						  git clone https://github.com/tpope/vim-surround.git
"						  --- Comments.vim : Easy commenting / uncommenting code 
"						  git clone https://github.com/vim-scripts/comments.vim.git
"						  --- Vim Lion : easy alignment around a caracter 
"						  git clone git://github.com/tommcdo/vim-lion.git 


" Ruby engine modification to go faster 
set re=1

" -------------------------------------------------
"  --- Initial Configuration : Color and tab
" -------------------------------------------------
" --- Color, and Scheme
set background=dark								" To have a dark and mysterious backgroud          
colorscheme jellybeans " Colorscheme : Have a look on jellybeans 
syntax enable									" Activate color syntax
set mouse=a										" enable popup for mousse
set mousemodel=popup  
" --- Number and Position in file
set number										" Adding line number
"set cursorline									" Hilight current line
"set cursorcolumn								" Hilight current column
set ruler										" Display current cursor position
" --- Indentation 
set autoindent									" Setting auto-indentation
set smartindent									" A smarter indentation is better !
set backspace=indent,eol,start					" Allow backspace 
set showcmd										" Showing current command
" --- Tabulations
set shiftwidth=4								" Size of Tabulation (number of space)
set softtabstop=2								" Tabulation and space size
set tabstop=4									" Displayed tabulation
" --- Finding Configuration
set incsearch									" Find when typing
set ignorecase									" By default, case is not taken into account when search
"set noic										" Case sensitive search 
set hlsearch									" Hilight search pattern
" --- Misc
set showmatch									" Automatic (,[ at typing
filetype plugin indent on						" Automatic file extension detection
set fileformats=unix,mac,dos					" autowrap for dos files
set autoread
" --- Disable bell for Macvim 
set vb t_vb=.



" -------------------------------------------------
" --- Backup System 
" -------------------------------------------------
if !filewritable ($HOME."/.vim/tmp")			" if backup does not exist, problem
call mkdir($HOME."/.vim/tmp", "p")			" backup dir
endif
set directory=$HOME/.vim/tmp					" Backup specification

" -------------------------------------------------
" --- Pathogen 
" -------------------------------------------------
execute pathogen#infect()



" -------------------------------------------------
" --- MapLeader : To have fast mapping 
" -------------------------------------------------
" --- Setting mapleader 
let mapleader = ","
let g:mapleader = ","
" --- Fast saving and quit
nmap <leader>w :w!<cr>
nmap <leader>q :wq<cr>
" --- Fast vimrc edition
map <leader>e :e! ~/.vimrc<cr>
" --- Fast reload of file
map <leader>r :e!%<cr>
" Automatic reload and source vimrc
autocmd! bufwritepost vimrc source ~/.vimrc

"Decrement on ctrl shit A
nnoremap <c-z> <c-x>
" --- tiping j and k switch to command mode
imap jk <ESC>







" -------------------------------------------------
" --- Open Close on vim 
" -------------------------------------------------
" --- Force vim to open at the previous editing place
set viminfo='10,\"100,:20,%,n~/.viminfo
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif
" --- Loading txt files as text file 
autocmd BufEnter *.txt set filetype=text


" -------------------------------------------------
" --- Buffers and Tabulars 
" -------------------------------------------------
" --- Fast Buffer navigation
map <leader>t :bp<cr>
map <leader>s :bn<cr>
" --- Fast tabular navigation
map <tab> gt
map <S-tab> gT
" --- Adding a Tab on f4
map <F4> :tabnew<cr>



" -------------------------------------------------
" --- Maping ! 
" -------------------------------------------------
" --- remove highlight after searching : ctrl + N !
nnoremap <silent> <C-N> :noh<CR>     
" --- Adding black line before current line in command mode
nnoremap <Space><Enter> o<ESC>						  
" --- Adding black line after current line in command mode
map <Enter> O<ESC>



" -------------------------------------------------
" --- cTags  
" -------------------------------------------------
" --- ctags repo
let tlist_ctags_cmd = '/opt/local/bin/ctags'
" --- Display ctags on F8 
nnoremap <silent> <F6> :TlistToggle<CR>
let Tlist_Process_File_Always = 1
" --- Latex Tags
let tlist_tex_settings   = 'latex;i:ToDo;s:sections;g:graphics;f:files;p:packages'
let tlist_make_settings  = 'make;m:makros;t:targets'
" --- if taglist window is the only window left, exit vim
let Tlist_Exit_OnlyWindow = 1 
" --- show tags of only one file
let Tlist_Show_One_File = 1 
" --- Tagbar parameters 
let g:tagbar_left = 1

" -------------------------------------------------
" --- Spell and Dictionnary  
" -------------------------------------------------
if has("spell")
	" --- Setting proposition counter to 10 [z= in command mode, <C-X-S>
	" in edition mode
	set spellsuggest=10
	" --- Mapping to enable / disable french dictionnary
	noremap ,sf :setlocal spell spelllang=fr <CR> | syntax spell toplevel <cr>
	" --- Mapping to enable / disable english dictionnary
	noremap ,se :setlocal spell spelllang=en <CR> | syntax spell toplevel <cr>
	" --- Disable spell checl
	noremap ,sn :setlocal nospell <CR>
	" --- rapid setting for Word
	imap <C-x><C-s>  <C-X><C-s>
endif  
" --- Lang of help
set helplang=fr


" -------------------------------------------------
" --- Status Line  
" -------------------------------------------------
set laststatus=2 " Always display status line
if has("statusline")
	set statusline=%t%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [FENC=%{&fileencoding}]\ [POS=%04l,%04v]\ [%P]\ [LEN=%L]\ [TAG=%{Tlist_Get_Tagname_By_Line()}]
elseif has("cmdline_info")
	set ruler " only display cursor
endif


"" -------------------------------------------------
"" --- Custom function for tab as completion (useless ?) 
"" -------------------------------------------------
"function! Tab_Or_Complete()
	"if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
		"return "\<C-N>"
	"else
		"return "\<Tab>"
	"endif
"endfunction
":inoremap <Tab> <C-R>=Tab_Or_Complete()<CR>


" -------------------------------------------------
" --- Vim and Latex  
" -------------------------------------------------
"" IMPORTANT: win32 users will need to have 'shellslash' set so that latex
"" can be called correctly.
set shellslash
"" --- Latex compilation
set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'
let g:Tex_DefaultTargetFormat='pdf'
let g:Tex_MultipleCompileFormats='pdf, aux'
"let g:Tex_ViewRule_pdf='okular'
let g:Tex_ViewRule_pdf = 'Skim'
let g:Tex_CompileRule_pdf = 'pdflatex -synctex=1  -file-line-error-style -interaction=nonstopmode  $*'
"" --- Open PFD file at position of cursor of latex file
function! SyncTexForward()
	let s:syncfile = fnamemodify(fnameescape(Tex_GetMainFileName()), ":r").".pdf"
	let execstr = "silent !open -a Skim %:r.pdf"
	"!okular --unique  ".s:syncfile."\\#src:".line(".").expand("%\:p").' &'
	exec execstr
endfunction
"nnoremap <Leader>o :call SyncTexForward()<CR>
map ,ls :w<CR>:silent !/Applications/Skim.app/Contents/SharedSupport/displayline -r <C-r>=line('.')<CR> %<.pdf %<CR><CR>

"" --- Fix the Ã© bug
imap <buffer> <leader>it <Plug>Tex_InsertItemOnThisLine

" -------------------------------------------------
" --- Search Word under cursor  
" -------------------------------------------------
" Search for selected text, forwards or backwards.
"Press * to search forwards for selected text, or # to search backwards. 
"As normal, press n for next search, or N for previous. 
"Handles multiline selection and search. 
"Whitespace in the selection matches any whitespace when searching (searching for "hello world" will also find "hello" at the end of a line, with "world" at the start of the next line).
"Each search is placed in the search history allowing you to easily repeat previous searches.
"No registers are changed
vnoremap <silent> * :<C-U>
			\let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
			\gvy/<C-R><C-R>=substitute(
			\escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
			\gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
			\let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
			\gvy?<C-R><C-R>=substitute(
			\escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
			\gV:call setreg('"', old_reg, old_regtype)<CR>


" -------------------------------------------------
" --- Matlab for VIM  
" -------------------------------------------------
" --- Setting compilation rule
autocmd BufEnter *.m    compiler mlint	  
" --- Press ,m to have a list of warnings and errors
map <leader>m make<cr>


" -------------------------------------------------
" --- NerdTree : Navigation files on F6  
" -------------------------------------------------
" --- Having nerd tree in left
"let g:NERDTreeWinPos = "left"
map <F5> :NERDTreeTabsToggle<cr>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
let g:nerdtree_tabs_open_on_gui_startup =0

" -------------------------------------------------
" --- Syntastic : Syntax checker  
" -------------------------------------------------
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" --- Parameter for syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" --- Deactivate for tex files 
let g:syntastic_mode_map = { 'mode': 'active',
                               \ 'passive_filetypes': ['tex']}

" --- Size of error window
let g:syntastic_loc_list_height=3

" Mapping for fast activation - deactivation
nmap <leader>sd :SyntasticToggleMode<cr>
nmap <leader>sa :SyntasticCheck<cr>


" -------------------------------------------------
" --- Miscallaneous  
" -------------------------------------------------
" --- Add wrapping to have words cut at the end sentences
set wrap linebreak
let &showbreak="\u21aa" 

" --- To switch to real line to displayed line when text is long
noremap <silent> <Leader>lw :call ToggleWrap()<CR>
function ToggleWrap()
	if &wrap
		echo "Wrap OFF"
		setlocal nowrap
		set virtualedit=all
		silent! nunmap <buffer> <Up>
		silent! nunmap <buffer> <Down>
		silent! nunmap <buffer> <Home>
		silent! nunmap <buffer> <End>
		silent! iunmap <buffer> <Up>
		silent! iunmap <buffer> <Down>
		silent! iunmap <buffer> <Home>
		silent! iunmap <buffer> <End>
	else
		echo "Wrap ON"
		setlocal wrap linebreak nolist
		set virtualedit=
		setlocal display+=lastline
		noremap  <buffer> <silent> <Up>   gk
		noremap  <buffer> <silent> <Down> gj
		noremap  <buffer> <silent> <Home> g<Home>
		noremap  <buffer> <silent> <End>  g<End>
		inoremap <buffer> <silent> <Up>   <C-o>gk
		inoremap <buffer> <silent> <Down> <C-o>gj
		inoremap <buffer> <silent> <Home> <C-o>g<Home>
		inoremap <buffer> <silent> <End>  <C-o>g<End>
	endif
endfunction



" --- autoclose quickfix window if last screen
au BufEnter * call MyLastWindow()
function! MyLastWindow()
	" if the window is quickfix go on
	if &buftype=="quickfix"
		" if this window is last on screen quit without warning
		if winbufnr(2) == -1
			quit!
		endif
	endif
endfunction


" --- Adding all bash feature for external commands
set shellcmdflag=-ic


" --- Adding () to a selected bloc
vnoremap (( <Esc> `>a)<Esc>`<i(<Esc>
vnoremap {{ <Esc> `>a}<Esc>`<i{<Esc>
vnoremap [[ <Esc> `>a]<Esc>`<i[<Esc>

" --- Deactivate autofolding with latex files
autocmd FileType tex setlocal  nofoldenable


" --- move words with CRTL
vmap <C-Right> xpgvlolo
vmap <C-left> xhPgvhoho
imap <C-a> jkA

" --- Police for gvim
set guifont=Monaco:h9


" --- Autoindent file and get back
map <F13> gg=G``


" --- Function for automacally close all buffers that are not open in vim
function! Wipeout()
	" list of *all* buffer numbers
	let l:buffers = range(1, bufnr('$'))

	" what tab page are we in?
	let l:currentTab = tabpagenr()
	try
		" go through all tab pages
		let l:tab = 0
		while l:tab < tabpagenr('$')
			let l:tab += 1

			" go through all windows
			let l:win = 0
			while l:win < winnr('$')
				let l:win += 1
				" whatever buffer is in this window in this tab, remove it from
				" l:buffers list
				let l:thisbuf = winbufnr(l:win)
				call remove(l:buffers, index(l:buffers, l:thisbuf))
			endwhile
		endwhile

		" if there are any buffers left, delete them
		if len(l:buffers)
			execute 'bwipeout' join(l:buffers)
		endif
	finally
		" go back to our original tab page
		execute 'tabnext' l:currentTab
	endtry
endfunction


" --- ctrl v to paste from real clipboard
nmap <c-v> "+p
imap <c-v> jk"+pa 

" --- Y for copy selection YY for copy line (clipboard : external paste enable)
vnoremap Y "+y
nnoremap YY "+yy
nnoremap Y "+y

" --- vim-airline to display buffers
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod= ':t'
" --- vim-airline buffers system Mapping
set hidden
nmap <leader>T :enew<cr>
nmap <c-tab> :bnext<cr>
nmap <c-s-tab> :bprevious <cr>
nmap <leader>Q :bd <cr>
nmap <leader>q :bd <cr>

" Explorer on :E and :e
nmap :E :Explore

" Shortcut to working directory
set autoread



" No automatic comment after comment line
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Completion in up to down mode
let g:SuperTabDefaultCompletionType = "<c-n>"
" Not Completion and apply tab after some keyword
"let g:SuperTabNoCompleteAfter = [',', '\s', ';', '^','%']


" Snipset Configuration
let g:UltiSnipsExpandTrigger="<c-e>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" --- Display Buffergator on F1 
nnoremap <silent> <F1> :BuffergatorToggle<CR>
nnoremap <silent> <F14> :BuffergatorToggle<CR>
