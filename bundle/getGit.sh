#!/bin/bash 
git clone https://github.com/scrooloose/nerdtree.git
git clone https://github.com/jistr/vim-nerdtree-tabs.git
git clone https://github.com/scrooloose/syntastic
git clone https://github.com/bling/vim-airline.git
git clone https://github.com/vim-latex/vim-latex.git 
git clone  https://github.com/vim-scripts/AutoComplPop.git
git clone https://github.com/ervandew/supertab.git
git clone https://github.com/sirver/ultisnips
git clone https://github.com/tpope/vim-surround.git
git clone https://github.com/vim-scripts/comments.vim.git
git clone git://github.com/tommcdo/vim-lion.git 
