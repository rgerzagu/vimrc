" ---------------------------
" Custom shortcuts for Julia 
" ----------------------------




" saving name of file in F5 to be launched in Julia session
"map <F5> :let  @+ = "include("."\"".expand("%:t:r").".jl\");" <cr>
map <F6> :SlimeSend "include("."\"".expand("%:p")."\");" <cr>
noremap <F4> :execute 'SlimeSend1 '.expand("%:t:r").'.main();' <cr>

nnoremap <F6> :execute 'SlimeSend1 include("'.expand('%:p').'");'<CR>
nnoremap <F5> :execute 'SlimeSend1 includet("'.expand('%:p').'");'<CR>

nnoremap <F3> :execute 'SlimeSend1 include("/home/root/dspChains/sshJulia/'.expand('%:h').'/'.expand('%:t').'")'<CR>


function! ToMain() 
 let moduleName = expand('%:p'); 
endfunction

function! DisplayInfo()
	let wordUnderCursor = expand("<cword>")
	let charJulia = 'SlimeSend1 println(varinfo(r"'.wordUnderCursor.'"))'
	exec charJulia
endfunction 
nmap <leader>v :call DisplayInfo()<cr>

function! DisplayVar()
	let wordUnderCursor = expand("<cword>")
	let charJulia = 'SlimeSend1 '.wordUnderCursor
	exec charJulia
endfunction 
nmap <leader>s :call DisplayVar()<cr>



imap /fun """ <cr>--- <++> <cr><cr># --- Syntax <cr><++><cr># --- Input parameters <cr>- <cr># --- Output parameters <cr>- <cr># --- <cr># v 1.0 - Robin Gerzaguet.<cr>"""



" Comments
imap ,# # --- 
imap ,## # ---------------------------------------------------- <cr># --- <++> <cr># ---------------------------------------------------- <cr><c-j>

" PGF Figure macro 



noremap <expr> <F7> LaTeXtoUnicode#Toggle()
inoremap <expr> <F7> LaTeXtoUnicode#Toggle()


" --- Mapping to type 
imap <leader>tac [Array{Complex{Float64}}]
imap <leader>taf [Array{Float64}]
imap <leader>tai [Array{Int}]
imap <leader>tc [Complex{Float64}]
imap <leader>tf [Float64]
imap <leader>ti [Int]
imap <leader>ts [String]
imap <leader>tu [Union{Int,Float64}]
