#/bin/bash

# --- Installation of plugin with pathogen !
mkdir ~/.vim/bundle
cd ~/.vim/bundle/ 
#---  nerdtred : easy navigation in subdir files (one tree to rule them all)
git clone https://github.com/scrooloose/nerdtree.git
git clone https://github.com/jistr/vim-nerdtree-tabs.git
#--- syntastic : on the fly checker 
git clone https://github.com/scrooloose/syntastic
#--- vim airline : fuzzy customized statebar 
git clone https://github.com/bling/vim-airline.git
#--- latex-suite : powerfull latex for vim
git clone https://github.com/vim-latex/vim-latex.git 
#--- acp : automatic completion frame
git clone  https://github.com/vim-scripts/AutoComplPop.git
#--- Supertab : smart completion on tab
git clone https://github.com/ervandew/supertab.git
#--- Ultisnips : automatic code completion
git clone https://github.com/sirver/ultisnips
