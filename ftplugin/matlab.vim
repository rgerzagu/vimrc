" saving name of file in F5 to be launched in Matlab
map <F5> :let  @+ =expand("%:t:r") <cr>


" --- Set shortcut for Matlab
imap ,% % --- 
imap /fig figure('name','<++>');<cr>hold on;<cr>grid minor;<cr>plot(<++><cr>xlabel('<++>');<cr>ylabel('<++>');<cr>title('<++>');<cr>
imap /fun % --- <++>.m <cr>% --- <cr>% Description <cr>%    <++><cr>% --- <cr>% Syntax <cr>%    <++><cr>% --- <cr>% v 1.0 - Robin Gerzaguet.
imap ,-- % ---------------------------------------------------- <cr>%% --- <++> <cr>% ---------------------------------------------------- <cr><c-j>
imap <leader>pp 'lineWidth',3,'LineStyle','-','color','red');


imap <leader>fp fprintf('<++>  %d \n',<++>);<c-j>


imap <leader>zz zeros(1,<++>);

function! SetBreakpoint()
	let s:syncfile	=  expand("%:t:r")
	let s:line		= line(".") 
	let execstr		= "dbstop in ".s:syncfile. " at ".s:line 
	let @+			= execstr
endfunction 
map <leader>dd :call SetBreakpoint()<cr>

function! EditFunction()
	let s:syncfile	=  expand("%:t:r")
	let execstr		= "edit  ".s:syncfile
	let @+			= execstr
endfunction 
map <F9> :call EditFunction()<cr>



imap <leader>beg % --- Clear all stuff<cr>vide;<cr>% --- Display function name<cr>disp(['simRF running function : ' mfilename('path')]);<cr>

"" section jumping
"noremap <buffer> <silent> ]] :<c-u>call TexJump2Section( v:count1, '' )<CR>
"noremap <buffer> <silent> [[ :<c-u>call TexJump2Section( v:count1, 'b' )<CR>
"function! TexJump2Section( cnt, dir )
  "let i = 0
  ""let pat = '^\s*\\\(part\|chapter\|\(sub\)*section\|paragraph\)\>\|\%$\|\%^'
  "let pat = '^\s*\(%% --- \)\>\|\%$\|\%^'
   "let flags = 'W' . a:dir
   "while i < a:cnt && search( pat, flags ) > 0
     "let i = i+1
   "endwhile
   "let @/ = pat
 "endfunction


noremap <buffer> <silent> [[ :let @/="%% ---"<cr> n
noremap <buffer> <silent> ]] :let @/="%% ---"<cr> N


